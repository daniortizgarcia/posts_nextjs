import React, { useState } from 'react'

import Layout from '../../components/layout/layout'
import styles from './contact.module.scss'

export default function Contact() {
  
  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [reason, setReason] = useState('');

  const sendForm = () => {
    console.log('name: ', name)
    console.log('surname: ', surname)
    console.log('email: ', email)
    console.log('phone: ', phone)
    console.log('reason: ', reason)
  } 

  return (
    <Layout>
      <div className={styles.contact}>
        <form className={styles.contact__form}>
          <fieldset>
            <label className={styles.contact__form_label} htmlFor="name">Nombre</label>
            <input className={styles.contact__form_input} type="text" id="name" placeholder="Nombre" value={name} onChange={(ev) => setName(ev.target.value)} />
          </fieldset>
          <fieldset>
            <label className={styles.contact__form_label} htmlFor="surname">Apellido</label>
            <input className={styles.contact__form_input} type="text" id="surname" placeholder="Apellido" value={surname} onChange={(ev) => setSurname(ev.target.value)} />
          </fieldset>
          <fieldset>
            <label className={styles.contact__form_label} htmlFor="email">Email</label>
            <input className={styles.contact__form_input} type="email" id="email" placeholder="Email" value={email} onChange={(ev) => setEmail(ev.target.value)} />
          </fieldset>
          <fieldset>
            <label className={styles.contact__form_label} htmlFor="phone">Teléfono</label>
            <input className={styles.contact__form_input} type="tel" id="phone" placeholder="Teléfono" value={phone} onChange={(ev) => setPhone(ev.target.value)} />
          </fieldset>
          <fieldset>
            <label className={styles.contact__form_label} htmlFor="reason">Motivo</label>
            <textarea className={styles.contact__form_placeholder} id="reason" value={reason} onChange={(ev) => setReason(ev.target.value)}></textarea>
          </fieldset>
          <fieldset>
            <button type="button" onClick={() => sendForm()}>Enviar</button>
          </fieldset>
        </form>
      </div>
    </Layout>
  )
}