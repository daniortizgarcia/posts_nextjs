import Layout from '../components/layout/layout'
import Posts from '../components/posts/posts'

 export default function Home () {
  return (
    <Layout>
      <Posts paginationNumbersShow={18} />
    </Layout>
  )
}
