import React, { useState, useEffect } from 'react'

import Post from './post'
import Pagination from '../pagination/pagination'
import styles from './posts.module.scss'

const Posts = ({ paginationNumbersShow }) => {
  const [posts, setPosts] = useState([]);
  const [postsBackup, setPostsBackup] = useState([]);
  const [pageSelected, setPage] = useState(0);
  const [paginationNumber, setPaginationNumber] = useState(0);

  useEffect(() => {
    if (!posts.length) {
      getPosts()
      if (!paginationNumber) {
        setPaginationNumber(Math.ceil(postsBackup.length / paginationNumbersShow)) 
      }
    }
    
    return () => {
      console.log("This will be logged on unmount");
    }
  });

  const getPosts = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    const json = await res.json()
    setPosts(json)
    setPostsBackup(json)
    pagination()
  }

  const pagination = (page = 0) => {
    const firstPost = page * paginationNumbersShow;
    setPosts(postsBackup.slice(firstPost, firstPost + paginationNumbersShow))
  }

  const changePagination = (page) => {
    setPage(page)
    pagination(page)
  }

  const listPost = posts.map((post) => <Post key={post.id} title={post.title} body={post.body} />)

  return (
    <div className={styles.posts}>
      <h2 className={styles.posts__title}>Posts</h2>
      <div className={styles.posts_container}>
        {listPost}
      </div>
      <div>
        <Pagination paginationNumber={paginationNumber} actualPage={pageSelected} changePagination={changePagination} />
      </div>
    </div>
  )
}

export default Posts
