import styles from './posts.module.scss';

export default function Post(post) {

  return (
    <div className={styles.posts_container__post}>
      <div className={styles.posts_container__content}>
        <h2 className={styles.posts_container__content_title}>{post.title}</h2>
        <p className={styles.posts_container__content_description}>{post.body}</p>
      </div>
    </div>
  )
}
