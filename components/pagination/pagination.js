import styles from './pagination.module.scss';

export default function Pagination({ paginationNumber, actualPage, changePagination}) {

  const listPages = paginationNumber ? Array(paginationNumber).fill(1).map((page, i) =>
    <div className={styles.pagination__number} onClick={() => changePagination(i)} key={i}>
      <button
        className={actualPage === i ?
          styles.pagination__number__button +' '+ styles.pagination__number__button_selected :
          styles.pagination__number__button}
      >
        {i + 1}
      </button>
    </div>
  ) : <div></div>
  
  return (
    <div className={styles.pagination}>
      <button 
        className={actualPage+1 <= 1 ? styles.pagination__prev +' '+ styles.pagination__prev_disabled : styles.pagination__prev}
        disabled={actualPage+1 <= 1}
        onClick={() => changePagination(actualPage - 1)}
      >
        prev
      </button>
      <div className={styles.pagination__numbers}>
        {listPages}
      </div>
      <button
        className={actualPage+1 >= paginationNumber ? styles.pagination__prev +' '+ styles.pagination__prev_disabled : styles.pagination__prev}
        disabled={actualPage+1 >= paginationNumber}
        onClick={() => changePagination(actualPage + 1)}
      >
        next
      </button>
    </div>
  )
}
