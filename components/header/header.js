import Link from 'next/link'
import styles from './header.module.scss'

export default function Header({ links }) {

  const listLinks = links.map((link) => 
    <li className={styles.header__list} key={link.id}>
      <Link href={link.link}><a className={styles.header__list__link}>{link.label}</a></Link>
    </li>
  )

  return (
    <header className={styles.header}>
      <div className={styles.header__icon}>
        <h2 className={styles.header__icon__title}>Logo</h2>
      </div>
      <ul className={styles.header__links}>{listLinks}</ul>
    </header>
  )
}
