import Head from 'next/head'
import Header from '../header/header'
import Footer from '../footer/footer'

 export default function Layout(props) {

  const links = [
    {id: 1, label: 'Inicio', link: '/'},
    {id: 2, label: 'Contacto', link: '/contact'}
  ]  

  return (
    <>
      <Head>
        <title>Inicio</title>
      </Head>
      <div className="container">
        <Header links={links} />
        <div id="main">{props.children}</div>
        <Footer />
      </div>
      <style jsx global>{`
        body {
          margin: 0;
        }
      `}
      </style>
    </>
  )
}
